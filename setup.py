#!/usr/bin/env python

from setuptools import setup
from setuptools import Command
import sys
import os
import shutil


PKG_NAME = "sklearn-relief"
MOD_NAME = PKG_NAME.replace('-', '_')
PKG_VERSION = "1.0.0b2"


class CmdLint(Command):
    """Run linter for source code or test files."""
    user_options = [
        ('test', 't', 'Run linter on test code')
    ]

    def initialize_options(self):
        self.test = None

    def finalize_options(self):
        self.test = bool(self.test)

    def run(self):
        test_files = (
            'test/test-relief.py',
        )
        src_files = (
            'src/sklearn_relief.py',
        )
        
        if self.test:
            files = test_files
            rcfile = 'pylintrc.tests'
        else:
            files = src_files
            rcfile = 'pylintrc'

        for f in files:
            os.system('pylint --rcfile=%s %s' % (rcfile, f))


class CmdClean(Command):
    """Clean distribution and intermediary files."""
    user_options = []
  
    def initialize_options(self):
        pass
  
    def finalize_options(self):
        pass
  
    def run(self):
        dirpath = os.path.dirname(__file__)
  
        # Remove directories
        for dirname in (
            'build',
            'dist',
            os.path.join('src', '{mod}.egg-info'.format(mod=MOD_NAME))
        ):
            shutil.rmtree(
                os.path.join(dirpath, dirname),
                ignore_errors=True
            )
  
        for filename in (
            'MANIFEST',
        ):
            pth = os.path.join(dirpath, filename)
  
            if os.path.exists(pth):
              os.unlink(pth)


setup(
    name=PKG_NAME,
    version=PKG_VERSION,
    description="Implementation of the Relief family of algorithms for feature ranking",
    author="Alfredo Mungo",
    author_email="alfredo.mungo@protonmail.ch",
    url="https://gitlab.com/qafir/sklearn-relief",
    python_requires=">=3.0",
    package_dir={'': 'src'},
    py_modules=[MOD_NAME],
    install_requires=[
        'numpy (>=1.13.3)',
        'scipy (>=1.0.0)',
        'scikit-learn (>=0.19.1)',
    ],
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Scientific/Engineering :: Information Analysis',
    ],
    cmdclass = {
      'clean': CmdClean,
      'lint': CmdLint
    }
)
