#!/usr/bin/env python

import warnings
import unittest
import os
import pandas as pd
import numpy as np
from sklearn.preprocessing import minmax_scale
from sklearn_relief import Relief, ReliefF, RReliefF


DATADIR = os.path.join(
    os.path.dirname(__file__),
    'data'
)


class ReliefCommonTestCaseMixin:
    """
    Mixin containing tests shared by all the algorithms of the Relief family.

    This Mixin requires:
        * `setUp()` to create a `self.params` dictionary
            containing the hyper-parameters to be passed to the Relief instance.
        * A class variable named `TEST_DATA_FILE_NAME` containing the file name
            of the test dataset.
        * A class variable named `TEST_DATA_LABEL_COLUMN_NAME` containing the
            column name of the label in the dataset.
        * A class variable named `ALGORITHM_CLASS` containing the class object
            to instantiate when testing single vs. multi-process runs.
        * A class variable named `TEST_DATA_EXPECTED_BEST_RANKED_COLUMN_INDEX`
            containing the index of the column expected as a result of the
            transform() method after testing its run.
        * A class variable named `TEST_DATA_EXPECTED_BEST_RANKED_COLUMN_THR`
            containing the threshold value for the absolute difference of each
            value from the resulting expected best-ranked column for asserting
            equal values.
        * An instance variable named `r` containing a newly instantiated
            `ALGORITHM_CLASS` object per test.

    Apart from the common test methods, this mixin will also provide:
        * A `load_dataset()` method to load the dataset from file and set the
            `data` and `y` class variables.
    """
    @classmethod
    def load_dataset(cls, dropna=True):
        """Load and set the required dataset from file"""
        df = pd.read_csv(os.path.join(DATADIR, cls.TEST_DATA_FILE_NAME))

        if dropna:
            df.dropna(inplace=True)

        label_name = cls.TEST_DATA_LABEL_COLUMN_NAME

        cls.data = df.drop(label_name, axis=1).as_matrix()
        cls.y = df[label_name].as_matrix()

    def test_single_process(self):
        if os.environ.get('RELIEF_TEST_ALGORITHM', 'n').lower() == 'y':
            self.r.fit(self.data, self.y)
            self.assertEqual(
                np.flip(self.r.w_.argsort(), 0)[0],
                self.TEST_DATA_EXPECTED_BEST_RANKED_COLUMN_INDEX
            )

            res = self.r.transform(self.data)
            self.assertTrue(
                np.all(
                    np.abs(
                        res.flatten()
                        - self.data[:, self.TEST_DATA_EXPECTED_BEST_RANKED_COLUMN_INDEX]
                    ) < self.TEST_DATA_EXPECTED_BEST_RANKED_COLUMN_THR
                )
            )

    def test_multiple_processes(self):
        if os.environ.get('RELIEF_TEST_ALGORITHM', 'n').lower() == 'y':
            self.params['n_jobs'] = os.cpu_count()
            r = self.ALGORITHM_CLASS(**self.params)

            r.fit(self.data, self.y)
            self.assertEqual(
                np.flip(r.w_.argsort(), 0)[0],
                self.TEST_DATA_EXPECTED_BEST_RANKED_COLUMN_INDEX
            )

            res = r.transform(self.data)
            self.assertTrue(
                np.all(
                    np.abs(
                        res.flatten()
                        - self.data[:, self.TEST_DATA_EXPECTED_BEST_RANKED_COLUMN_INDEX]
                    ) < self.TEST_DATA_EXPECTED_BEST_RANKED_COLUMN_THR
                )
            )

    def test_param_check(self):
        with self.assertRaises(ValueError):
            self.ALGORITHM_CLASS(non_existing_param=5)
    
    def test_random_state_param(self):
        r = self.ALGORITHM_CLASS(random_state=666)

        self.assertEqual(r.random_state.randint(10), 2)

        r = self.ALGORITHM_CLASS(
            random_state = np.random.RandomState(seed=666)
        )

        self.assertEqual(r.random_state.randint(10), 2)


class ReliefTestCase(unittest.TestCase, ReliefCommonTestCaseMixin):
    TEST_DATA_FILE_NAME = 'titanic.csv'
    TEST_DATA_LABEL_COLUMN_NAME = 'Survived'
    TEST_DATA_EXPECTED_BEST_RANKED_COLUMN_INDEX = 8
    TEST_DATA_EXPECTED_BEST_RANKED_COLUMN_THR = 1
    ALGORITHM_CLASS = Relief

    @classmethod
    def setUpClass(cls):
        cls.load_dataset()

    def setUp(self):
        self.params = dict(
            n_iterations=300,
            n_features=1,
            categorical=[2, 3, 7, 9, 10],
            n_jobs=1,
            random_state=666
        )
        self.r = self.ALGORITHM_CLASS(**self.params)

    def test_diff(self):
        i1 = np.array([1, 3, 2, 1])
        i2 = np.array([0, 2, 1, 0])
        res = np.array([1, 1, 1, 1])

        for c in range(i1.shape[0]):
            self.assertEqual(self.r._diff(c, i1[c], i2[c]), res[c])

        r = self.ALGORITHM_CLASS(
            categorical=[1, 2]
        )
        i1 = np.array([1, 'a', 'a', 1], dtype=object)
        i2 = np.array([0, 'b', 'a', 0], dtype=object)
        res = np.array([1, 1, 0, 1])

        for c in range(i1.shape[0]):
            self.assertEqual(r._diff(c, i1[c], i2[c]), res[c])

    def test_nn(self):
        r = self.ALGORITHM_CLASS()
        data = np.array([
            [1, 1, 1, 1],
            [1, 2, 1, 5],
            [1, 1, 1, 1],
            [1, 2, 5, 1],
            [1, 3, 2, 1]
        ])
        y = np.array([0, 0, 0, 1, 1]).T
        h, m = r._nn(data, y, 0)

        self.assertTrue(np.all(h == data[2]))
        self.assertTrue(np.all(m == data[4]))


class ReliefFTestCase(unittest.TestCase, ReliefCommonTestCaseMixin):
    TEST_DATA_FILE_NAME = 'iris.csv'
    TEST_DATA_LABEL_COLUMN_NAME = 'Species'
    TEST_DATA_EXPECTED_BEST_RANKED_COLUMN_INDEX = 3
    TEST_DATA_EXPECTED_BEST_RANKED_COLUMN_THR = .1
    ALGORITHM_CLASS = ReliefF

    @classmethod
    def setUpClass(cls):
        cls.load_dataset()

    def setUp(self):
        self.params = dict(
            n_iterations=300,
            n_features=1,
            n_jobs=1,
            random_state=666
        )
        self.r = self.ALGORITHM_CLASS(**self.params)

    def test_class_frequencies(self):
        yexp = {
            'Iris-setosa': 50. / 150.,
            'Iris-virginica': 50. / 150.,
            'Iris-versicolor': 50. / 150.,
        }

        self.assertDictEqual(self.r._class_frequencies(self.y), yexp)

    def test_isna(self):
        v = {
            0: False,
            1: False,
            '': False,
            'a': False,
            None: True,
            np.nan: True,
            'nan': False
        }

        for x, y in v.items():
            self.assertEqual(self.r._isna(x), y)

    def test_knn(self):
        r = self.ALGORITHM_CLASS(categorical=[3])
        data = np.array([
            [1, 1, 1, 'a'],
            [1, 1, 2, 'b'],
            [1, 1, 2, 'c'],
            [2, 2, 2, 'd'],
            [1, 1, 2, 'a'],
            [1, 1, 1, 'a']
        ], dtype=object)
        y = np.array([1, 1, 2, 2, 3, 3])
        knn = r._knn(data, y, 0, [1, 2, 3])
        exp_res = {
            1: [1],
            2: [2, 3],
            3: [5, 4]
        }

        for c, nns in knn.items():
            for nni, nn in enumerate(nns):
                self.assertEqual(nn, exp_res[c][nni])

    def test_frequency(self):
        r = self.ALGORITHM_CLASS(approx_decimals=2)
        data = np.array([1, 2, .0001, .001, .009, 10, .01])

        self.assertAlmostEqual(r._frequency(1, data), 1. / data.shape[0])
        self.assertAlmostEqual(r._frequency(.01, data), 2. / data.shape[0])
        self.assertAlmostEqual(r._frequency(.001, data), 0)

        data = np.array(['a', 'b', 'c'], dtype=object)
        self.assertAlmostEqual(r._frequency('c', data), 1. / 3.)

    def test_diff_ramp_params_constraints(self):
        with self.assertRaises(ValueError):
            self.ALGORITHM_CLASS(ramp=True, tdiff=.1, teq=1)

    def test_diff_ramp(self):
        r = self.ALGORITHM_CLASS(ramp=True, tdiff=1, teq=.1)

        self.assertAlmostEqual(r._diff_ramp(5, 3), 1)
        self.assertAlmostEqual(r._diff_ramp(5, 4.99), 0)
        self.assertAlmostEqual(r._diff_ramp(5, 4.5), 0.44, places=2)

    def test_diff_value(self):
        r = self.ALGORITHM_CLASS(categorical=[3])
        data = np.array([
            [1, 1, 1, 'a', np.nan],
            [1, 2, 1, 'a', 5],
            [1, 1, 1, 'b', 5],
            [1, 1, 1, 'b', 4],
        ], dtype=object)
        y = np.array([1, 1, 1, 2])

        self.assertEqual(r._diff_value(0, 0, 1, data, y), 0)
        self.assertEqual(r._diff_value(1, 0, 1, data, y), 1)
        self.assertEqual(r._diff_value(1, 1, 1, data, y), 0)
        self.assertEqual(r._diff_value(3, 1, 1, data, y), 0)
        self.assertAlmostEqual(r._diff_value(4, 0, 1, data, y), .33, places=2)
        self.assertAlmostEqual(r._diff_value(4, 0, 0, data, y), 1. - (2. / 3.) ** 2, places=2)

    def test_diff_one_nan(self):
        r = self.ALGORITHM_CLASS(categorical=[3])
        data = np.array([
            [1, 1, 1, 'a', np.nan],
            [1, 2, 1, 'a', 5],
            [1, 1, 1, 'b', 5],
            [1, 1, 1, 'b', 4],
        ], dtype=object)
        y = np.array([1, 1, 1, 2])

        self.assertAlmostEqual(r._diff_one_nan(4, 1, 0, data, y), .33, places=2)

    def test_diff_both_nan(self):
        r = self.ALGORITHM_CLASS(categorical=[3])
        data = np.array([
            [1, 1, 1, 'a', np.nan],
            [1, 2, 1, 'a', 5],
            [1, 1, 1, 'b', 5],
            [1, 1, 1, 'b', 4],
        ], dtype=object)
        y = np.array([1, 1, 1, 2])

        self.assertAlmostEqual(r._diff_both_nan(4, 0, 0, data, y), 1. - (2. / 3.) ** 2, places=2)

    def test_diff(self):
        r = self.ALGORITHM_CLASS(categorical=[3, 5])
        data = np.array([
            [1, 1, 1, 'a', 5, 'c'],
            [1.5, 1, 1, 'b', 4, 'c'],
        ], dtype=object)
        y = np.array([1, 2])

        self.assertEqual(r._diff(0, 1, data, y), 2.5)


class RReliefFTestCase(unittest.TestCase, ReliefCommonTestCaseMixin):
    TEST_DATA_FILE_NAME = 'housing.csv'
    TEST_DATA_LABEL_COLUMN_NAME = 'median_house_value'
    TEST_DATA_EXPECTED_BEST_RANKED_COLUMN_INDEX = 2
    TEST_DATA_EXPECTED_BEST_RANKED_COLUMN_THR = .1
    ALGORITHM_CLASS = RReliefF

    @classmethod
    def setUpClass(cls):
        cls.load_dataset()

        with warnings.catch_warnings():
            warnings.simplefilter('ignore')
            data_num = minmax_scale(np.delete(cls.data, 8, 1))

        data_str = cls.data[:, 8]
        data_str = data_str.reshape((data_str.shape[0], 1))

        cls.data = np.hstack((data_num, data_str))

    def setUp(self):
        self.params = dict(
            sigma=100,
            k=100,
            n_iterations=200,
            n_features=1,
            n_jobs=1,
            categorical=[8],
            random_state=666
        )
        self.r = self.ALGORITHM_CLASS(**self.params)

    def test_knn(self):
        r = self.ALGORITHM_CLASS(k=3, categorical=[3])
        data = np.array([
            [1, 1, 1, 'a'], # 0
            [1, 2, 2, 'c'], # 1
            [2, 2, 2, 'd'], # 2
            [1, 1, 2, 'a'], # 3
            [1, 1, 1, 'a'] # 4
        ], dtype=object)
        y = np.array([1, 1, 2, 2, 3, 3])
        knn = r._knn(data, y, 0)
        exp_res = [4, 3, 1]

        self.assertEqual(len(knn), 3)

        for nn, exp_nn in zip(knn, exp_res):
            self.assertEqual(nn, exp_nn)


if __name__ == '__main__':
    unittest.main(verbosity=2)
